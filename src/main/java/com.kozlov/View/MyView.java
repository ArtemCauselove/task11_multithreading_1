package com.kozlov.View;

import com.kozlov.controller.Controller;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView {
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);
    Controller controller = new Controller();
    private static Logger logger = LogManager.getLogger(MyView.class);

    public MyView() {
        menu = new LinkedHashMap<>();
        methodsMenu = new LinkedHashMap<>();
        menu.put("1", "  1 - Ping Pong game");
        menu.put("2", "  2 - Fibonacci with different executors");
        menu.put("3", "  3 - Fibonacci sum");
        menu.put("4", "  4 - Sleepnig task");
        menu.put("5", "  5 - Critical task");
        menu.put("6", "  6 - Pipe communication");
        menu.put("Q", "  Q - exit");
        methodsMenu.put("1", this::pingPongGame);
        methodsMenu.put("2", this::fibonacceExec);
        methodsMenu.put("3", this::sumFinonacci);
        methodsMenu.put("4", this::sleepingTask);
        methodsMenu.put("5", this::criticals);
        methodsMenu.put("6", this::pipeThread);
    }
    private void pingPongGame(){
        controller.pingPong();
    }
    private void fibonacceExec(){
        controller.fibonacciExecutors();
    }
    private void sumFinonacci(){
        controller.fibonacciCallable();
    }
    private void sleepingTask(){
        logger.info("Input number of treads");
        int num = input.nextInt();
        controller.sleepTime(num);
    }
    private void criticals (){
        controller.criticalSecion();
    }
    private void pipeThread(){
        controller.pipeCommunication();
    }

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values())
            logger.info(str + "\n");
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            logger.info("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }
}
