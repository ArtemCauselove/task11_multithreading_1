package com.kozlov.model;

import java.util.concurrent.*;

public  class FibonacciExample extends Thread {
    volatile int  n1 = 0;
    volatile int n2 = 1;
    volatile int n3 = 0;
    volatile int length;
    private static Object sync = new Object();
   public  FibonacciExample(int length) {
        this.length = length;
    }
    public  FibonacciExample() { }
    @Override
    public void  run() {
        synchronized(sync){
        int i = 0;
        int sum = n1+n2;
        System.out.print(n1 + " " + n2);//printing 0 and 1
        while (i < length) {

            n3 = n1 + n2;
            n1 = n2;
            n2 = n3;
            System.out.print(" " + n3);
            i++;
            sum = sum + n3;
        }}
        System.out.println(" " );
    }
        public void begin()  {
        FibonacciExample first = new FibonacciExample(3);
        FibonacciExample second = new FibonacciExample(5);
        FibonacciExample third = new FibonacciExample(10);
        FibonacciExample fourth = new FibonacciExample(15);
        FibonacciExample fifth = new FibonacciExample(20);
        ExecutorService executorSc = Executors.newSingleThreadExecutor();
        Executor executor = Executors.newSingleThreadExecutor();
        ScheduledExecutorService executorServiceSched = Executors.newSingleThreadScheduledExecutor();
            executor.execute(first);
            executor.execute(second);
            executor.execute(third);
            executorSc.submit(fourth);
            executorServiceSched.schedule(fifth,2, TimeUnit.SECONDS);

/*        t2.start();
        t3.start();
        t2.join();
        t3.join();*/
        }
    }
