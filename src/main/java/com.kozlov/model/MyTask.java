package com.kozlov.model;

import java.time.LocalDateTime;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class MyTask extends Thread {
    private static Runnable runnabledelayedTask = new Runnable()
    {
        @Override
        public void run()
        {
            System.out.print("Start of sleep" + LocalDateTime.now());
            try {
                TimeUnit.SECONDS.sleep((int) (Math.random()*10));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("    End of sleep" + LocalDateTime.now());
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } };
    public  void begin(int number)  {
        ScheduledExecutorService scheduledPool = Executors.newScheduledThreadPool(number);
        scheduledPool.scheduleWithFixedDelay(runnabledelayedTask, 1, 1, TimeUnit.SECONDS);
    }
}