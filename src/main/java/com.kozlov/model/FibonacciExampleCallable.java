package com.kozlov.model;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public  class FibonacciExampleCallable extends Thread {
    int  n1 = 0;
    int n2 = 1;
    int n3 = 0;
    int length;
    private static Object sync = new Object();
    public   FibonacciExampleCallable() { }
    public   FibonacciExampleCallable(int length) {
        this.length = length;
    }
    Callable<Integer> callable = () -> {

        int i = 0;
        int sum = n1+n2;
        System.out.print(n1 + " " + n2);
        while (i < length) {
            n3 = n1 + n2;
            n1 = n2;
            n2 = n3;
            System.out.print(" " + n3);
            i++;
            sum = sum + n3;
        }
        System.out.println(" " );
        return  sum;
    };
    @Override
    public void run(){

    }
        public void begin() {
        FibonacciExampleCallable first = new FibonacciExampleCallable(3);
        FibonacciExampleCallable second = new FibonacciExampleCallable(5);
        FibonacciExampleCallable third = new FibonacciExampleCallable(10);
        FibonacciExampleCallable fourth = new FibonacciExampleCallable(15);
        FibonacciExampleCallable fifth = new FibonacciExampleCallable(20);
            List<FibonacciExampleCallable> list = new ArrayList<>();
            list.add(first);
            list.add(second);
            list.add(third);
            list.add(fourth);
            list.add(fifth);
            ExecutorService executorService = Executors.newSingleThreadExecutor();
            for(FibonacciExampleCallable current: list) {
                Future<Integer> future = executorService.submit(current.callable);
                Integer result = null;
                try {
                    result = future.get();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
                System.out.println("sum is :" + result);
            }
        }
    }
