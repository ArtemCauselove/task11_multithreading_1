package com.kozlov.controller;

import com.kozlov.model.*;

import java.io.IOException;

public class Controller {
    PingPong pingPong = new PingPong();
    ConnectWithPipe pipe = new ConnectWithPipe();
    FibonacciExample fibo = new FibonacciExample();
    FibonacciExampleCallable fiboCall = new FibonacciExampleCallable();
    MyTask sleepTime = new MyTask();
    SyncronizingExample syncEx = new SyncronizingExample();

    public void pingPong(){
        pingPong.begin();
    }
    public void fibonacciExecutors()  {
        fibo.begin();
    }
    public void fibonacciCallable() {
        fiboCall.begin();
    }
    public void sleepTime(int number)  {
        sleepTime.begin(number);
    }
    public void criticalSecion(){
        syncEx.begin();
    }
    public void pipeCommunication()  {
        try {
            pipe.begin();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
